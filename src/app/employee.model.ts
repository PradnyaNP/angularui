export class EmployeeDetails{
    id?: number;
    name?:string;
    department?:string;
    joining_date?:Date;
  }